// Return a String as an Integer

// Create a function that takes a string and returns it as an integer.

// stringInt("6") ➞ 6

// stringInt("1000") ➞ 1000

// stringInt("12") ➞ 12

// solutions
function stringInt(str) {
  return str * 1;
}

const stringInt = function stringInt(str) {
  return Number(str);
};
