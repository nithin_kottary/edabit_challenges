// Check String for Spaces
// Create a function that returns true if a string contains any spaces.

function isSpaces(str) {
  return str.indexOf(" ") != -1;
}
