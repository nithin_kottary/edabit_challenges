// Lexicographically First and Last

// Write a function that returns the lexicographically first and lexicographically last rearrangements of a lowercase string. Output the results in the following manner:

// firstAndLast(string) ➞ [first, last]

// firstAndLast("marmite") ➞ ["aeimmrt", "trmmiea"]

// firstAndLast("bench") ➞ ["bcehn", "nhecb"]

// firstAndLast("scoop") ➞ ["coops", "spooc"]

function firstAndLast(str) {
  const arr = str.split("");
  const first = arr.sort().join("");
  const last = arr.reverse().join("");
  return [first, last];
}
