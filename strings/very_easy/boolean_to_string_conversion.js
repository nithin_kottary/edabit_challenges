// Boolean to String Conversion

// Create a function that takes a boolean variable flag and returns it as a string.

// Examples
// boolToString(true) ➞ "true"

// boolToString(false) ➞ "false"

// soluitons
// 1. regular function
function boolToString(value) {
  return value + "";
}

// functione expressions
const boolToStringExpr = function (value) {
  return value.toString();
};

// arrow function
const boolToStringArrow = (value) => String(value);

boolToString(true);
boolToStringExpr(true);
boolToStringArrow(true);
