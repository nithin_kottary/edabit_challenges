// Basic Variable Assignment

// A student learning JavaScript was trying to make a function. His code should concatenate a passed string name with string "Edabit" and store it in a variable called result. He needs your help to fix this code.

// nameString("Mubashir") ➞ "MubashirEdabit"

// nameString("Matt") ➞ "MattEdabit"

// nameString("javaScript") ➞ "javaScriptEdabit"

// soluitons
// 1. regular function
function nameString(str) {
  return `${str}Edabit`;
}

// functione expressions
const nameStringExpression = function (str) {
  return `$ ${str}Edabit`;
};

// arrow function
const nameStringArrow = (str) => `${str}Edabit`;

nameString(`Nithin`);
nameStringExpression(`Nithin`);
nameStringArrow(`Nithin`);
