// Return Something to Me!

// Write a function that returns the string "something" joined with a space " " and the given argument a.

// giveMeSomething("is better than nothing") ➞ "something is better than nothing"

// giveMeSomething("Bob Jane") ➞ "something Bob Jane"

// giveMeSomething("something") ➞ "something something"

// soluitons
// 1. regular function
function giveMeSomething(name) {
  return `${something} ${name}`;
}

// functione expressions
const hellgiveMeSomethingExpr = function () {
  return `${something} ${name}`;
};

// arrow function
const giveMeSomethingArrow = (name) => `${something} ${name}`;

giveMeSomething("Nithin");
hellgiveMeSomethingExpr("Nithin");
hellgiveMeSomethingArrow("Nithin");
