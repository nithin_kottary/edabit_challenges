// Is the Word Singular or Plural?

// Create a function that takes in a word and determines whether or not it is plural. A plural word is one that ends in "s".

// isPlural("changes") ➞ true

// isPlural("change") ➞ false

// isPlural("dudes") ➞ true

// isPlural("magic") ➞ false

function isPlural(word) {
  const lastCharecter = word.slice(-1);
  return lastCharecter === 's';
}
