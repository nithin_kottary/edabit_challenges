// Name Greeting!

// Create a function that takes a name and returns a greeting in the form of a string. Don't use a normal function, use an arrow function.

// helloName("Gerald") ➞ "Hello Gerald!"

// helloName("Tiffany") ➞ "Hello Tiffany!"

// helloName("Ed") ➞ "Hello Ed!"

function helloName(name) {
  return `Hello ${name}`;
}
