// Buggy Code (Part 4)

// Emmy has written a function that returns a greeting to users. However, she's in love with Mubashir, and would like to greet him slightly differently. She added a special case in her function, but she made a mistake.

// greeting("Matt") ➞ "Hello, Matt!"

// greeting("Helen") ➞ "Hello, Helen!"

// greeting("Mubashir") ➞ "Hello, my Love!"

// their code given in the code tab
// function greeting(name) {
//     return "Hello, " + name + "!";
//     if(name == "Mubashir") {
//       return "Hello, my Love!";
//     }
//   }

// solution for their code

// function greeting(name) {
//   if (name == "Mubashir") {
//     return "Hello, my Love!";
//   }
//   return "Hello, " + name + "!";
// }

// my solution

function greeting(firstName) {
  return `Hello, ${firstName === "Nithin" ? "my love" : firstName}`;
}
