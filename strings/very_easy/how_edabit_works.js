// How Edabit Works

// This is an introduction to how challenges on Edabit work. In the Code tab above you'll see a starter function that looks like this:

// function hello() {

// }

// All you have to do is type return "hello edabit.com" between the curly braces { } and then click the Check button. If you did this correctly, the button will turn red and say SUBMIT FINAL. Click it and see what happens.

// soluitons
// 1. regular function
function hello() {
  return "hello edabit.com";
}

// functione expressions
const helloExpression = function () {
  return "hello edabit.com";
};

// arrow function
const helloArrow = () => `Hello world`;

hello();
helloExpression();
helloArrow();