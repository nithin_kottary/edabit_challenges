// Concatenating First and Last Character of a String

// Create a function that takes a string and returns the concatenated first and last character.

// firstLast("ganesh") ➞ "gh"

// firstLast("kali") ➞ "ki"

// firstLast("shiva") ➞ "sa"

// firstLast("vishnu") ➞ "vu"

// firstLast("durga") ➞ "da"

// using slice
function firstLast(str) {
  const firstChar = str.slice(0, 1);
  const lastChar = str.slice(-1);
  return firstChar + lastChar;
}

// using charAt
const firstLastExpr = function (str) {
  const firstChar = str.charAt(0);
  const lastCharIndex = str.length - 1;
  const lastChar = str.charAt(lastCharIndex);
  return firstChar + lastChar;
};
