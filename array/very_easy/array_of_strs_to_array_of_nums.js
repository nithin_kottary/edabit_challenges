// Array of Strings to Array of Numbers

// Create a function that takes as a parameter an array of "stringified" numbers and returns an array of numbers.

// ["1", "3", "3.6"] ➞ [1, 3, 3.6]

function Stringify(arr) {
  return arr.map((element) => Number(element));
}
