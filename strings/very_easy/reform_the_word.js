// Re-Form the Word

// A word has been split into a left part and a right part. Re-form the word by adding both halves together, changing the first character to an uppercase letter.

// getWord("seas", "onal") ➞ "Seasonal"

// getWord("comp", "lete") ➞ "Complete"

// getWord("lang", "uage") ➞ "Language"

function getWord(first, second) {
  return `${first}${second}`;
}
